package com.example.agendasearch;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> bufferList;
    private int originalIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundle = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundle.getSerializable("contactos");
        bufferList = contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }

    public void cargarContactos(){
        for(int x = 0; x < contactos.size(); x++){
            final Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(this);
            TextView nText = new TextView(this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,10);
            nText.setTextColor((c.isFavorito())? Color.BLUE:Color.BLACK);
            nText.setGravity(Gravity.CENTER);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 8);
            nButton.setTextColor(Color.rgb(56,60,74));

            Button borrar = new Button(ListActivity.this);
            borrar.setText(R.string.accver);
            borrar.setTextSize(TypedValue.COMPLEX_UNIT_PT, 8);
            borrar.setTextColor(Color.rgb(56,60,74));

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i = 0; i<bufferList.size(); i++){
                        if(c.getNombre().equals(bufferList.get(i).getNombre()) && c.getTelefono1().equals(bufferList.get(i).getTelefono1())){
                            Contacto c = (Contacto) view.getTag(R.string.contacto_g);
                            Intent intent = new Intent();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("contacto",c);
                            bundle.putInt("index",i);
                            intent.putExtras(bundle);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                }
            });

            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    for(int i=0;i<bufferList.size();i++){
                        if(c.getNombre().equals(bufferList.get(i).getNombre()) &&
                                c.getTelefono1().equals(bufferList.get(i).getTelefono1())){
                            bundle.putInt("id",i);
                        }
                    }
                    bundle.putBoolean("delete",true);
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });



            nButton.setTag(R.string.contacto_g,c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);

            tblLista.addView(nRow);
        }
    }

    public void buscar(String busqueda) {
        ArrayList<Contacto> filterList = new ArrayList<>();
        for (int i=0; i<bufferList.size(); i++){
            if(bufferList.get(i).getNombre().toUpperCase().contains(busqueda.toUpperCase())){
                filterList.add(bufferList.get(i));
            }
        }

        contactos = filterList;
        tblLista.removeAllViews();
        cargarContactos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}